/**
 * Created by Lukasz on 2016-11-18.
 */
"use strict"

angular.module('blogfontApp.services', []).factory('Blog', function ($resource) {


    return {
      Allpost:  $resource('http://localhost:8080/api/blog'),
      Lastpost:  $resource('http://localhost:8080/api/blog/showlast')
      }

});
