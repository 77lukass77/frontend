'use strict';

/**
 * @ngdoc overview
 * @name blogfontApp
 * @description
 * # blogfontApp
 *
 * Main module of the application.
 */
angular
  .module('blogfontApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngResource',
    'blogfontApp.controllers',
    'blogfontApp.services'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'BlogListController',
        controllerAs: 'blog'
      })
      .when('/add', {
        templateUrl: 'views/add.html',
        controller: 'BlogAddController',
        controllerAs: 'addpost'
      })
      .when('/showlast', {
        templateUrl: 'views/showlast.html',
        controller: 'BlogLastController',
        controllerAs: 'showlast'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
