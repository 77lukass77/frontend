'use strict';

/**
 * @ngdoc function
 * @name blogfontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the blogfontApp
 */
angular.module('blogfontApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
