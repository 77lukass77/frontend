'use strict';

/**
 * @ngdoc function
 * @name blogfontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the blogfontApp
 */
angular.module('blogfontApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
