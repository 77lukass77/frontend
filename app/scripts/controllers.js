/**
 * Created by Lukasz on 2016-11-18.
 */

"use strict"


angular.module('blogfontApp.controllers', []).controller('BlogListController', function ($scope, Blog) {


      $scope.bloglist = Blog.Allpost.query();
      console.log($scope.bloglist);

  }).controller('BlogAddController', function ($scope, Blog, ngDialog) {

    $scope.blog = new Blog();

    $scope.addBlog=function () {
      $scope.blog.$save(function () {
          console.log("Wpis dodany");
           ngDialog.open({ template: 'views/popup.html', className: 'ngdialog-theme-default' });
      })
    }

}).controller('BlogLastController', function ($scope, Blog) {

     $scope.bloglast = Blog.Lastpost.get();

});
